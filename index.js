const nools = require("nools");
const redis = require('redis');
const {promisify} = require('util');

let client = redis.createClient();
const hmGetAllAsync = promisify(client.hgetall).bind(client);

async function main() {
    let Rule = await hmGetAllAsync("rules");

    let helloRuleJSON = JSON.parse(Rule.Hello)
    let goodbyeRuleJSON = JSON.parse(Rule.Goodbye)

    let ruleHelloFunc = new Function('return ' + helloRuleJSON.ruleFunction)();

    let flow = nools.flow("Hello World", function (flow) {

        // find any message that is exactly hello world
        flow.rule(helloRuleJSON.ruleName, [ruleHelloFunc, helloRuleJSON.param, helloRuleJSON.rule], function (facts) {
            facts.m.text = facts.m.text + " goodbye";
            this.modify(facts.m);
        });


        flow.rule(goodbyeRuleJSON.ruleName, [ruleHelloFunc, goodbyeRuleJSON.param, goodbyeRuleJSON.rule], function (facts) {
            console.log(facts.m.text);
        });
    });

    var session = flow.getSession();

    var m = new ruleHelloFunc("hello world");

    session.assert(m);

    //now fire the rules
    session.match().then(
        function(){
            console.log("Done");
        },
        function(err){
            //uh oh an error occurred
            console.error(err.stack);
        }
    );

    client.unref();    
}

main();