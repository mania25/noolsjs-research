const redis = require("redis");

const client = redis.createClient();

let ruleData = {
    "ruleName": "Hello",
    "param": "m",
    "rule": "m.text =~ /^hello\\sworld$/",
    "ruleFunction": function(message) {
        this.text = message;
    }
};

let ruleDataStringify = JSON.stringify(ruleData, function (key, value) {
    if (typeof value === 'function') {
        return value.toString();
      } else {
        return value;
      }
});

let ruleData2 = {
    "ruleName": "Goodbye",
    "param": "m",
    "rule": "m.text =~ /.*goodbye$/",
    "ruleFunction": function(message) {
        this.text = message;
    }
};

let ruleDataStringify2 = JSON.stringify(ruleData2, function (key, value) {
    if (typeof value === 'function') {
        return value.toString();
      } else {
        return value;
      }
});

client.hmset("rules", "Hello", ruleDataStringify, "Goodbye", ruleDataStringify2, function(err, res){
    console.log(err, res);
});